# Docker Lunch & Learn - Django

You'll find here the example used on the presentation.

## Instructions
### How to run
- Run `docker-compose up`

### How to run commands inside container
- Example: `docker-compose run web django-admin.py startproject composeexample .`
